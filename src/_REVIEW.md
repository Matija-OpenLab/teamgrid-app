# 9.2. b
[] importy najprv vueckove, potom extra packages, potom vlastne
[] vyhodit bodkociarky
[] pouzivat jednoite uvodzovky v js
[] aj v css davat medzeru `.rule {`
[] pouzivat scss/sass vsade, vnarat pravidla podla real hierarchie
[] aj tu medzeru `components: {`
[] cele fake api by som vyhodil, lebo miesas logiku co to ma robit s nejakou zvlastnou custom funkcionalitou ze ked fetchujes url tak sa to vyhodnoti, oddel ten simulator, sprav z toho peknu classu ktora si to odklada do localStoragu
    [] also, sprav to cez store, store sluzi na drzanie globalnych veci, a potom ked bude backendova logika, tak nebudes musiet ani zasahovat do appky, iba do storu dorobis integraciu na backend, bude to future-ready
    [] hlavne to nerob cez ten axios ked to nema nic spolocne s axiosom


# 9.2.
[] store dat do pluginu
[] theme dat do app/_theme
[] pred zlozenu zatvorku funkcie davat medzeru - `fn() {` a `if() {`
[] aj za if davat medzeru - `if ()`
[] osobne by som nerobil ten simulator cez axios a interceptor, lebo ty teraz chces vyslovene custom vec, spravil by som si axpress, alebo apiSimulator ktory si includnes a to bude simulator
    - moxios je spraveny hentak, pretoze predpokladame ze tie api budu identicke a ze v mockoch mame identicku strukturu ako api, cize potom to uz treba len prepnut, ale ty riesis inu situaciu
