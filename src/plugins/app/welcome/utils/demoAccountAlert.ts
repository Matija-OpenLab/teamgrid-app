import { alertController } from '@ionic/vue'
import $store from '@/store'
import $router from '@/plugins/app/_config/router'

export const demoAccountAlert = () => {
    return alertController
    .create({
        header: 'Demo account detected',
        message: 'You have demo account registered locally. Do you want to proceed with it?',
        buttons: [
        {
            text: 'No',
            role: 'cancle',
            },
            {
                text: 'Yes',
                role: 'yes',
                handler: async () => {
                    await $store.dispatch('loginUser')
                    $router.push({name: 'Home'})
                }
            }
        ]
    })
    .then((a: any) => a.present())
}