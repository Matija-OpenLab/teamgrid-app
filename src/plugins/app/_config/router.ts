import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';


const route = (name: string) => {
  return {
    path: name,
    name: name.charAt(0).toUpperCase() + name.slice(1),
    component: () => import(`@/plugins/app/${name}/${name}.vue`)
  }
}

// const layout = (path: string, layoutPath: string, children: Array<any>) => {
//   return {
//     path: `/${path}`,
//     component: () => import(layoutPath),
//     children: children
//   }
// }

const routes: Array<RouteRecordRaw> = [
  {
    path: '',
    redirect: '/welcome'
  },
  {
    path: '/',
    component: () => import('@/plugins/app/_layouts/default.vue'),
    children: [
      route('welcome'),
      route('demo')
    ]
  },
  {
    path: '/app',
    component: () => import('@/plugins/app/_layouts/a-header.vue'),
    children: [
      route('home'),
      route('projects')
    ]
  }

]


const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
