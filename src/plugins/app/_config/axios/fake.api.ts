import { uuid } from "vue-uuid";
// import moment from 'moment'

const $ls = {
    get(name) {
        return JSON.parse(localStorage.getItem(name))
    },
    set(name, data) {
        const lsData = $ls.get(name)
    
        //checks if LS entry exists, if it does it just adds new data, if not it initiates it
        localStorage.setItem(name, JSON.stringify(lsData ? [...lsData, data] : [data]))
    },
    remove(name) {
        localStorage.removeItem(name)
    }
}

//dynamic api helpers
function putTasks(taskId, taskData) {
    const tasks = $ls.get('fake-tasks')
    const task = tasks.filter(task => task.id == taskId)[0]
    tasks[tasks.indexOf(task)] = taskData
    localStorage.setItem('fake-tasks', JSON.stringify(tasks))
}
function putProject(projectId, projectData) {
    const projects = $ls.get('fake-projects')
    const project = projects.filter(project => project.id == projectId)[0]
    projects[projects.indexOf(project)] = projectData
    localStorage.setItem('fake-projects', JSON.stringify(projects))
}
function getTaskById(taskId) {
    return $ls.get('fake-tasks').filter(task => task.id == taskId)[0]
}
function getProjectById(projectId) {
    return $ls.get('fake-projects').filter(project => project.id == projectId)[0]
}

export const staticEndpoints = {
    'get /tasks': () => {
        return $ls.get('fake-tasks') ? $ls.get('fake-tasks') : [] 
    },

    'post /tasks': (taskData) => {
        taskData.id = uuid.v1()
        taskData.info = {
            totalTime: '00:00:00'
        }
        $ls.set('fake-tasks', taskData)
    },
    'get /projects': () => {
        return $ls.get('fake-projects') ? $ls.get('fake-projects') : []
    },

    'post /projects': (projectData) => {
        projectData.id = uuid.v1()
        $ls.set('fake-projects', projectData)
    },

    'post /times': (timeData) => {
        $ls.set('fake-times', timeData)
    },

    'post /register': (userData) => {
        return {
            name: userData.name,
            id: uuid.v1()
        }
    }
}
export const dynamicEndpoints = (url, param, data?) => {
    switch(url) {
        case 'post /tasks//startTracking/': 
            $ls.set('fake-currentTrackingTask', {task: getTaskById(param.join()), ...data})
            break
        case 'post /tasks//stopTracking/':
            $ls.remove('fake-currentTrackingTask')
            break
        case 'get /tasks/':
            return getTaskById(param.join())
        case 'put /tasks/': 
            putTasks(param.join(), data)
            break
        case 'get /projects/':
            return getProjectById(param.join())
        case 'put /projects/': 
            putProject(param.join(), data)
            break
    }
}

