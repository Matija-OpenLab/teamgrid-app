import {toastController} from '@ionic/vue'

export const presentToast = (message: string) => {
  return toastController
        .create({
            header: message,
            color: 'dark',
            duration: 1500,
        }).then(toast => toast.present())
}
