import axios from 'axios'
import { staticEndpoints, dynamicEndpoints } from '@/plugins/app/_config/axios/fake.api'

function _processRequestLocally(config) {
    let resp = null
    let url = ''
    if (/\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/g.test(config.url)) {
        
        url = config.url.match(/\/\D{1,}\//g).join('')
        const param = config.url.match(/\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/g)

        resp = dynamicEndpoints(`${config.method} ${url}`, param, config.data)
    } else {
        resp = staticEndpoints[`${config.method} ${config.url}`](config.data)
    }
    const c = {
        method: 'get',
        url: '',
    }
    return {...config, ...c, fakeData: resp}
}

axios.interceptors.request.use(config => _processRequestLocally(config))

axios.defaults.baseURL = ''

if(!localStorage.getItem('fake-projects'))axios.post('/projects', {name: 'test'})

// axios.get('/tasks')
// axios.post('/tasks', { name: 'jebat iich' })
// async function test() {
//     console.log("shoudl start tracking");
//     const id = 'e27987a0-623b-11eb-882e-9561537c09d9'
//     const data = await axios.post(`/tasks/${id}/startTracking/`)
    
// }
// test()