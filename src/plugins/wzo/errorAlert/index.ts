import {alertController} from '@ionic/vue'

export const alertError = (error: any, callBack?: any) => {
  return alertController
    .create({
      header: 'Error!',
      message: error.message,
      buttons: [
        {
          text: 'Retry',
          role: 'Ok',
          handler: () => {
            if(callBack)callBack()
          }
        },
      ]
    })
    .then((a: any) => a.present())
}
