import axios from 'axios'

const state = {
    demoUser: localStorage.getItem('demoUser') || {}
}

const mutations = {
    userCreated(state , user) {
        state.demoUser = user
        localStorage.setItem('demoUser', JSON.stringify(user))
    },
    userLoggedIn(state) {
        state.demoUser = localStorage.getItem('demoUser')
    }
}

const actions = {
    async createUser({ commit }, userData) {
        const {config: {fakeData: data}} = await axios.post('/register', userData)
        commit('userCreated', data)
    },
    loginUser({ commit }) {
        commit('userLoggedIn')
    }
}

export default { state, mutations, actions }