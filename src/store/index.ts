import { createStore } from "vuex" 
import demoAuth from '@/store/modules/demoAuth.module'

const store = createStore({
    modules: {
        demoAuth
    }
})

export default store